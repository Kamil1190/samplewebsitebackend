﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SampleWebsite.Backend.Models
{
    public class MenuItem
    {
        [Required]
        public string name { get; set; }

        [Required]
        public string url { get; set; }

        public virtual ICollection<MenuItem> child { get; set; }
    }
}