﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SampleWebsite.Backend.Models.Dto
{
    public class MenuItemDto
    {
        [Required]
        public string name { get; set; }

        [Required]
        public string url { get; set; }

        public bool showChild { get; set; }
        public List<MenuItemDto> child { get; set; }
    }
}