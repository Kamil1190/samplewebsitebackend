﻿using System.ComponentModel.DataAnnotations;

namespace SampleWebsite.Backend.Models.Dto
{
    public class ServiceDto
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string Type { get; set; }
        [Required]
        public string Description { get; set; }
    }
}