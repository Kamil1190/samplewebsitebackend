﻿using AutoMapper;
using SampleWebsite.Backend.Models;
using SampleWebsite.Backend.Models.Dto;
using SampleWebsite.Domain.Entites;

namespace SampleWebsite.Backend.App_Start
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<MenuItem, MenuItemDto>().ForMember(dto => dto.name, opt => opt.MapFrom(x => x.name))
                .ForMember(dto => dto.url, opt => opt.MapFrom(x => x.url))
                .ForMember(dto => dto.showChild, opt => opt.UseValue(false))
                .ForMember(dto => dto.child, opt => opt.MapFrom(x => x.child));

            CreateMap<Service, MenuItemDto>().ForMember(dto => dto.name, opt => opt.MapFrom(x => x.Name))
                .ForMember(dto => dto.showChild, opt => opt.UseValue(false))
                .ForMember(dto => dto.url, opt => opt.MapFrom(x => "#/service/" + x.Id))
                .ForMember(dto => dto.child, opt => opt.AllowNull());

            CreateMap<Service, ServiceDto>();
            CreateMap<ServiceDto, Service>();
        }

    }
}