
[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(SampleWebsite.Backend.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(SampleWebsite.Backend.App_Start.NinjectWebCommon), "Stop")]

namespace SampleWebsite.Backend.App_Start
{
    using System;
    using System.Web;
    using Microsoft.Web.Infrastructure.DynamicModuleHelper;
    using Ninject;
    using Ninject.Web.Common;
    using System.Web.Http;
    using Moq;
    using Ninject.Web.WebApi;
    using Domain.Entites.Abstract;
    using Domain.ImitationRepository;
    using Domain.Repository;

    public static class NinjectWebCommon 
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start() 
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }
        
        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }
        
        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                RegisterServices(kernel);
                GlobalConfiguration.Configuration.DependencyResolver = new NinjectDependencyResolver(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            //bind mock to singleton
            //Mock<IServiceRepository> mock = new Mock<IServiceRepository>();
            //mock.Setup(s => s.Services).Returns(ServiceRepository.GetAll());
            //kernel.Bind<IServiceRepository>().ToConstant(mock.Object);

            //bind EF
            kernel.Bind<IServiceRepository>().To<EFServiceRepository>();
            kernel.Bind<IAuthRepository>().To<EFAuthRepository>();
        }        
    }
}
