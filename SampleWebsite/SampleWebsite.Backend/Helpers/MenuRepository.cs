﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using SampleWebsite.Backend.Models;
using SampleWebsite.Backend.Models.Dto;
using SampleWebsite.Domain.Entites;

namespace SampleWebsite.Backend.Helpers
{
    public class MenuRepository
    {
        public static List<MenuItem> GetBasic()
        {
            List<MenuItem> items = new List<MenuItem>();
            List<MenuItem> childItems = new List<MenuItem>();

            childItems.Add(new MenuItem { name = "About Site", url = "#/aboutSite" });
            childItems.Add(new MenuItem { name = "About Me", url = "#/aboutMe" });

            items.Add(new MenuItem { name = "About", url = "#/about", child = childItems });
            items.Add(new MenuItem { name = "Services", url = "#/services" });
            items.Add(new MenuItem { name = "Clients", url = "#/clients" });
            items.Add(new MenuItem { name = "Contact", url = "#/contact" });

            return items;
        }




        public static List<MenuItemDto> GetBasicDtos()
        {
            List<MenuItemDto> dots = GetBasic().Select(Mapper.Map<MenuItem, MenuItemDto>).ToList();
            return dots;
        }


        public static List<MenuItemDto> GetDtos(List<Service> services)
        {
            List<MenuItemDto> dots = GetBasicDtos();

            MenuItemDto serviceItemDto = dots.FirstOrDefault(i => i.name == "Services");
            if (serviceItemDto != null)
            {
                serviceItemDto.child = services.Select(Mapper.Map<Service, MenuItemDto>).ToList();
            }

            //MAP ALL CHILDS
            dots.ForEach(x => x.child.ForEach(y => y = Mapper.Map<MenuItemDto>(y)));
            return dots;
        }


    }
}