﻿using Microsoft.AspNet.Identity;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.AspNet.Identity.EntityFramework;
using SampleWebsite.Domain.Entites;
using SampleWebsite.Domain.Entites.Abstract;
using SampleWebsite.Domain.Repository;

namespace SampleWebsite.Backend.Controllers
{
    [RoutePrefix("api/Account")]
    public class AccountController : ApiController
    {
        private IAuthRepository repo;
        public AccountController(IAuthRepository repo)
        {
            this.repo = repo;
        }


        public async Task<IHttpActionResult> Register(User userModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            IdentityResult result = await repo.RegisterUser(userModel);
            IHttpActionResult errorResult = GetErrorResult(result);
            if (errorResult != null)
            {
                return errorResult;
            }
            return Ok();
        }

        [Authorize]
        [HttpGet]
        public IHttpActionResult Information()
        {
            string userName = RequestContext.Principal.Identity.Name;
            return Ok(userName);
        }


        private IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }

                if (ModelState.IsValid)
                {
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }
            return null;
        }
    }
}
