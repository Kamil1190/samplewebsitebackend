﻿using System.Web.Http;
using SampleWebsite.Domain.Entites.Abstract;
using SampleWebsite.Domain.Repository;

namespace SampleWebsite.Backend.Controllers
{
    [RoutePrefix("api/RefreshTokens")]
    public class RefreshTokensController : ApiController
    {
        private IAuthRepository repo;

        public RefreshTokensController(IAuthRepository repo)
        {
            this.repo = repo;
        }
    }
}
