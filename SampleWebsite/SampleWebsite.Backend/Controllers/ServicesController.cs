﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using AutoMapper;
using SampleWebsite.Backend.Models.Dto;
using SampleWebsite.Domain.Entites;
using SampleWebsite.Domain.Entites.Abstract;
using ServiceDto = SampleWebsite.Backend.Models.Dto.ServiceDto;

namespace SampleWebsite.Backend.Controllers
{
    [RoutePrefix("api/Services")]
    [Authorize]
    public class ServicesController : ApiController
    {
        private readonly IServiceRepository repository;

        public ServicesController(IServiceRepository repository)
        {
            this.repository = repository;
        }

        //GET /api/services
        [HttpGet, Route("")]
        public IHttpActionResult Get()
        {
            return Ok(repository.Services);
        }

        //POST /api/services    JSON: { "Name": "Service2", "Type": "typ2", "Description": "costam" }
        [HttpPost, Route("")]
        public IHttpActionResult Post([FromBody] ServiceDto serviceDto)
        {
            if (ModelState.IsValid)
            {
                repository.SaveService(Mapper.Map<Service>(serviceDto));
                return StatusCode(HttpStatusCode.Created);
            }
            return BadRequest();
        }

        //DELETE /api/services?id=2
        [HttpDelete, Route("")]
        public IHttpActionResult Delete(int id)
        {
            if (repository.DeleteService(id) != null)
            {
                return Ok();
            }
            return NotFound();
        }
    }
}
