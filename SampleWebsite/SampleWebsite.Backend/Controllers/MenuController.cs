﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using SampleWebsite.Backend.Helpers;
using SampleWebsite.Backend.Models.Dto;
using SampleWebsite.Domain.Entites.Abstract;

namespace SampleWebsite.Backend.Controllers
{
    [RoutePrefix("api/Menu")]
    public class MenuController : ApiController
    {
        private IServiceRepository repository;
        private List<MenuItemDto> itemsDtos;

        public MenuController(IServiceRepository repository)
        {
            this.repository = repository;
        }


        [Route("GetBasic")]
        [AllowAnonymous]
        public IHttpActionResult GetBasic()
        {
            itemsDtos = MenuRepository.GetBasicDtos();
            return Ok(itemsDtos);
        }


        [Route("Get")]
        [Authorize]
        public IHttpActionResult Get()
        {
            itemsDtos = MenuRepository.GetDtos(repository.Services.ToList());
            return Ok(itemsDtos);
        }


    }
}
