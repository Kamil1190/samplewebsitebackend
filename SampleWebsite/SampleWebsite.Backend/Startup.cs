﻿using Microsoft.Owin;
using Owin;
using System;
using System.Diagnostics;
using System.Reflection;
using System.Web.Http;
using AutoMapper;
using Microsoft.Owin.Security.OAuth;
using Ninject;
using SampleWebsite.Backend.App_Start;
using SampleWebsite.Backend.Providers;

[assembly: OwinStartup(typeof(SampleWebsite.Backend.Startup))]
namespace SampleWebsite.Backend
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            HttpConfiguration config = new HttpConfiguration();
            WebApiConfig.Register(config);

            //NINJECT
            config.DependencyResolver = new Ninject.Web.WebApi.NinjectDependencyResolver(new Ninject.Web.Common.Bootstrapper().Kernel);
            //AUTOMAPPER
            Mapper.Initialize(c => c.AddProfile<MappingProfile>());
            //AUTH
            ConfigureOAuth(app);
            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);

            app.UseWebApi(config);

            Debug.WriteLine("App start");
        }
        

        public void ConfigureOAuth(IAppBuilder app)
        {
            OAuthAuthorizationServerOptions OAuthServerOptions = new OAuthAuthorizationServerOptions()
            {
                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString("/token"),
                AccessTokenExpireTimeSpan = TimeSpan.FromMinutes(10), //test for 10 min
                Provider = new SimpleAuthorizationServerProvider(),
                RefreshTokenProvider = new SimpleRefreshTokenProvider()
            };

            app.UseOAuthAuthorizationServer(OAuthServerOptions);
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());
        }


    }
}