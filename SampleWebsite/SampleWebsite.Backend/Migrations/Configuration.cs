using SampleWebsite.Domain;

namespace SampleWebsite.Backend.Migrations
{
    using Domain.Entites.Auth;
    using Domain.Models;
    using Domain.Repository;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ApplicationDbContext context)
        {
            InitialData.Seed(ref context);
            base.Seed(context);
        }
    }
}
