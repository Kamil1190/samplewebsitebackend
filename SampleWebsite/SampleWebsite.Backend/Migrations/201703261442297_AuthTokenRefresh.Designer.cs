// <auto-generated />
namespace SampleWebsite.Backend.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.0-30225")]
    public sealed partial class AuthTokenRefresh : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AuthTokenRefresh));
        
        string IMigrationMetadata.Id
        {
            get { return "201703261442297_AuthTokenRefresh"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
