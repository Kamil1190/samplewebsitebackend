﻿using System.ComponentModel.DataAnnotations;

namespace SampleWebsite.Domain.Entites
{
    public class User
    {
        [Required]
        public string userName { get; set; }

        [Required]
        public string password { get; set; }

        [Required]
        public string confirmPassword { get; set; }
    }
}