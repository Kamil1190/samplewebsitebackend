﻿using System.Collections.Generic;

namespace SampleWebsite.Domain.Entites.Abstract
{
    public interface IServiceRepository
    {
        IEnumerable<Service> Services { get; }
        void SaveService(Service service);
        Service DeleteService(int serviceId);
    }
}