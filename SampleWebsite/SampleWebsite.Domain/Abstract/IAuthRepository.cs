﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using SampleWebsite.Domain.Entites;
using SampleWebsite.Domain.Entites.Auth;

namespace SampleWebsite.Domain.Entites.Abstract
{
    public interface IAuthRepository
    {
        Task<IdentityResult> RegisterUser(User userModel);
        Task<IdentityUser> FindUser(string userName, string password);
        Client FindClient(String clientId);
        IEnumerable<IdentityUser> GetAllUsers();
        Task<bool> AddRefreshToken(RefreshToken token);
        Task<bool> RemoveRefreshToken(string refreshTokenId);
        Task<bool> RemoveRefreshToken(RefreshToken token);
        Task<RefreshToken> FindRefreshToken(string tokenId);
    }
}
