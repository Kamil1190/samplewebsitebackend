﻿using System.Collections.Generic;
using SampleWebsite.Domain.Entites;

namespace SampleWebsite.Domain.ImitationRepository
{
    public class ServiceRepository
    {
        public static List<Service> GetAll()
        {
            List<Service> list = new List<Service>
            {
                new Service { Id=1, Name = "service1", Type = "typ1", Description = "Description service 1" },
                new Service { Id=2, Name = "service2", Type = "typ2", Description = "Description service 2" },
                new Service { Id=3, Name = "service3", Type = "typ3", Description = "Description service 3" },
                new Service { Id=4, Name = "service4", Type = "typ4", Description = "Description service 4" }
            };
            return list;
        }
    }
}