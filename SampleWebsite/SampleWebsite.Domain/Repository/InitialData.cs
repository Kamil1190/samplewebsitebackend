﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using SampleWebsite.Domain.Entites.Auth;
using SampleWebsite.Domain.Models;

namespace SampleWebsite.Domain.Repository
{
    public static class InitialData
    {
        public static void Seed(ref ApplicationDbContext context)
        {
            //Register angular app in database
            Client ngApp = new Client()
            {
                Id = "ngApp",
                Secret = "1234567890",
                Name = "AngularJS Application",
                ApplicationType = ApplicationTypes.JavaScript,
                Active = true,
                RefreshTokenLifeTime = 7200,
                //AllowedOrigin = "http://localhost:51627"      //for a website included in this solution
                AllowedOrigin = "http://localhost:8080"         //for a website outside solution
            };
            context.Clients.Add(ngApp);

            //New test User Username: test, Password: test12
            IdentityUser user = new IdentityUser
            {
                UserName = "test"
            };
            UserManager<IdentityUser> userManager = new UserManager<IdentityUser>(new UserStore<IdentityUser>(context));
            userManager.Create(user, "test12");
            context.SaveChanges();
        }
    }
}
