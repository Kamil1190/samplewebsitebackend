﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using SampleWebsite.Domain.Entites;
using SampleWebsite.Domain.Entites.Abstract;
using SampleWebsite.Domain.Entites.Auth;

namespace SampleWebsite.Domain.Repository
{
    public class EFAuthRepository : IAuthRepository, IDisposable
    {
        private ApplicationDbContext db { get; set; }
        private UserManager<IdentityUser> userManager;

        public EFAuthRepository()
        {
            db = new ApplicationDbContext();
            userManager = new UserManager<IdentityUser>(new UserStore<IdentityUser>(db));
        }

        public async Task<IdentityResult> RegisterUser(User userModel)
        {
            IdentityUser user = new IdentityUser
            {
                UserName = userModel.userName
            };

            var result = await userManager.CreateAsync(user, userModel.password);

            return result;
        }

        public async Task<IdentityUser> FindUser(string userName, string password)
        {
            IdentityUser user = await userManager.FindAsync(userName, password);

            return user;
        }

        public Client FindClient(String clientId)
        {
            return db.Clients.Find(clientId);
        }

        public IEnumerable<IdentityUser> GetAllUsers()
        {
            List<IdentityUser> users = userManager.Users.ToList();
            return users;
        }


        //REFRESH TOKENS

        public async Task<bool> AddRefreshToken(RefreshToken token)
        {
            RefreshToken existing = db.RefreshTokens.SingleOrDefault(t => t.Subject == token.Subject && t.ClientId == token.ClientId);
            if (existing != null)
            {
                await RemoveRefreshToken(existing);
            }
            db.RefreshTokens.Add(token);
            return await db.SaveChangesAsync() > 0;
        }

        public async Task<bool> RemoveRefreshToken(string refreshTokenId)
        {
            var refreshToken = await db.RefreshTokens.FindAsync(refreshTokenId);
            if (refreshToken != null)
            {
                db.RefreshTokens.Remove(refreshToken);
                return await db.SaveChangesAsync() > 0;
            }
            return false;
        }

        public async Task<bool> RemoveRefreshToken(RefreshToken token)
        {
            db.RefreshTokens.Remove(token);
            return await db.SaveChangesAsync() > 0;
        }

        public async Task<RefreshToken> FindRefreshToken(string tokenId)
        {
            return await db.RefreshTokens.FindAsync(tokenId);
        }

        public List<RefreshToken> GetAllRefreshTokens()
        {
            return db.RefreshTokens.ToList();
        }

        public void Dispose()
        {
            db.Dispose();
            userManager.Dispose();
        }
    }
}