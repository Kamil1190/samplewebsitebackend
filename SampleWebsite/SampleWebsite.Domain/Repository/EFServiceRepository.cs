﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SampleWebsite.Domain.Entites;
using SampleWebsite.Domain.Entites.Abstract;

namespace SampleWebsite.Domain.Repository
{
    public class EFServiceRepository: IServiceRepository
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public IEnumerable<Service> Services
        {
            get { return db.Services; }
        }

        public void SaveService(Service service)
        {
            if (service.Id == 0)
            {
                db.Services.Add(service);
            }
            else
            {
                Service serviceDb = db.Services.Find(service.Id);
                if (serviceDb != null)
                {
                    serviceDb.Name = service.Name;
                    serviceDb.Type = service.Type;
                    serviceDb.Description = service.Description;
                }
            }
            db.SaveChanges();
        }

        public Service DeleteService(int serviceId)
        {
            Service serviceDb = db.Services.Find(serviceId);
            if (serviceDb != null)
            {
                db.Services.Remove(serviceDb);
                db.SaveChanges();
            }
            return serviceDb;
        }
    }
}
