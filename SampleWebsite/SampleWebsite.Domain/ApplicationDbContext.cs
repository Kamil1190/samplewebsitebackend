using System.Data.Entity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using SampleWebsite.Domain.Entites;
using SampleWebsite.Domain.Entites.Auth;
using SampleWebsite.Domain.Models;
using System.Diagnostics;
using SampleWebsite.Domain.Repository;

namespace SampleWebsite.Domain
{
    public class ApplicationDbContext : IdentityDbContext<IdentityUser>
    {
        //AUTH
        public DbSet<Client> Clients { get; set; }
        public DbSet<RefreshToken> RefreshTokens { get; set; }



        public ApplicationDbContext()
            : base("name=ApplicationDbContext")
        {
            Database.SetInitializer<ApplicationDbContext>(new AuthDBInitializer());
        }

        public DbSet<Service> Services { get; set; }



    }


    public class AuthDBInitializer : CreateDatabaseIfNotExists<ApplicationDbContext>
    {
        protected override void Seed(ApplicationDbContext context)
        {
            InitialData.Seed(ref context);
            base.Seed(context);
        }
    }

}