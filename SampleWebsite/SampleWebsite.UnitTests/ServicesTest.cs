﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http.Results;
using AutoMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SampleWebsite.Backend.App_Start;
using SampleWebsite.Backend.Controllers;
using SampleWebsite.Backend.Models.Dto;
using SampleWebsite.Domain.Entites;
using SampleWebsite.Domain.Entites.Abstract;
using SampleWebsite.Domain.ImitationRepository;
using SampleWebsite.UnitTests.Compares;

namespace SampleWebsite.UnitTests
{
    [TestClass]
    public class ServicesTest
    {
        [ClassInitialize]
        public static void Init(TestContext context)
        {
            //INITIALIZE AUTOMAPPER FOR TEST
            Mapper.Initialize(c => c.AddProfile<MappingProfile>());
        }

        [TestMethod]
        public void Get_Contains_All_Items()
        {
            Mock<IServiceRepository> mock = new Mock<IServiceRepository>();
            mock.Setup(s => s.Services).Returns(ServiceRepository.GetAll());
            ServicesController controller = new ServicesController(mock.Object);

            //run
            var actionResult = controller.Get();
            
            //assert
            var response = actionResult as OkNegotiatedContentResult<IEnumerable<Service>>;
            Assert.IsNotNull(response);
            var beforeList = ServiceRepository.GetAll().ToList();
            var aftertList = response.Content.ToList();
            CollectionAssert.AreEqual(beforeList, aftertList, new ServiceComparer());
        }

        [TestMethod]
        public void Can_Save_Valid_Changes()
        {
            Mock<IServiceRepository> mock = new Mock<IServiceRepository>();
            ServicesController controller = new ServicesController(mock.Object);
            Service service = new Service()
            {
                Name = "test",
                Type = "test",
                Description = "Test"
            };
            ServiceDto serviceDto = Mapper.Map<ServiceDto>(service);

            //run
            var actionResult = controller.Post(serviceDto);

            //assert
            mock.Verify(m => m.SaveService(It.Is<Service>(s => s.Name == service.Name 
                                                            && s.Type == service.Type 
                                                            && s.Description == service.Description)));   //check if repo call :)
            Assert.IsNotInstanceOfType(actionResult, typeof(BadRequestResult));
        }

        [TestMethod]
        public void Cannot_Save_Invalid_Changes()
        {
            Mock<IServiceRepository> mock = new Mock<IServiceRepository>();
            ServicesController controller = new ServicesController(mock.Object);
            ServiceDto service = new ServiceDto()
            {
                Name = "test",
                Type = "test",
                Description = "Test"
            };
            controller.ModelState.AddModelError("error","error");

            //run
            var actionResult = controller.Post(service);

            //assert
            mock.Verify(m => m.SaveService(It.IsAny<Service>()), Times.Never);   //check if repo call :)
            Assert.IsInstanceOfType(actionResult, typeof(BadRequestResult));
        }

        [TestMethod]
        public void Can_Delete_valid_Products()
        {
            Service serviceToDelete = ServiceRepository.GetAll().First();

            Mock<IServiceRepository> mock = new Mock<IServiceRepository>();
            mock.Setup(s => s.Services).Returns(ServiceRepository.GetAll());

            //run
            ServicesController controller = new ServicesController(mock.Object);
            controller.Delete(serviceToDelete.Id);

            //assert
            mock.Verify(s => s.DeleteService(serviceToDelete.Id));
        }
    }
}
