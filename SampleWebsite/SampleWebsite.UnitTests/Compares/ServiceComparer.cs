﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SampleWebsite.Domain.Entites;

namespace SampleWebsite.UnitTests.Compares
{
    class ServiceComparer : Comparer<Service>
    {
        public override int Compare(Service x, Service y)
        {
            if (x.Id.CompareTo(y.Id) != 0)
                return -1;

            if (x.Name.CompareTo(y.Name) != 0)
                return -1;

            if (x.Description.CompareTo(y.Description) != 0)
                return -1;

            if (x.Type.CompareTo(y.Type) != 0)
                return -1;
            return 0;
        }
    }
}
